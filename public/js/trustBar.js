!function (e, t) {
    "use strict";
    function a(e) {
        if ("undefined" == typeof jQuery || 1 === parseInt(jQuery.fn.jquery) && parseFloat(jQuery.fn.jquery.replace(/^1\./, "")) < 10) {
            var a = t.getElementsByTagName("head")[0], r = t.createElement("script");
            r.src = ("https:" == t.location.protocol ? "https://" : "http://") + "ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js", r.type = "text/javascript", r.onload = r.onreadystatechange = function () {
                r.readyState ? "complete" !== r.readyState && "loaded" !== r.readyState || (r.onreadystatechange = null, e(jQuery.noConflict(!0))) : e(jQuery.noConflict(!0))
            }, a.appendChild(r)
        } else e(jQuery)
    }

    a(function (e) {
        e("head").append("" +
            "<link href='https://cdnjs.cloudflare.com/ajax/libs/paymentfont/1.2.5/fonts/paymentfont-webfont.svg' rel='stylesheet'>" +
            "<link href='https://cdnjs.cloudflare.com/ajax/libs/paymentfont/1.2.5/fonts/paymentfont-webfont.ttf' rel='stylesheet'>" +
            "<link href='https://cdnjs.cloudflare.com/ajax/libs/paymentfont/1.2.5/fonts/paymentfont-webfont.eot' rel='stylesheet'>" +
            "<link href='https://cdnjs.cloudflare.com/ajax/libs/paymentfont/1.2.5/fonts/paymentfont-webfont.woff' rel='stylesheet'>" +
            "<link href='https://cdnjs.cloudflare.com/ajax/libs/paymentfont/1.2.5/css/paymentfont.min.css' rel='stylesheet'>" +
            "<style type='text/css'>div.ba-trust-wrapper{  padding-top:15px;}div.ba-trust-wrapper h3{        font-size: 26px;  }div.ba-trust-wrapper ul{  margin:0px;  padding:0px;}div.ba-trust-wrapper i{      font-size: 32px;    }div.ba-trust-wrapper ul li{  display:inline-block;  padding-right:10px;}</style>");
        var t = e('form[action*="/cart/add"]').first();
        if (0 == e(".ba-trust-wrapper").length) {
            var a = "<div class='ba-trust-wrapper'><h3>Checkout securely with</h3>";
            a += "<ul>", a += "<li><i class='pf pf-visa'></i></li>", a += "<li><i class='pf pf-mastercard-alt'></i></li>", a += "<li><i class='pf pf-american-express'></i></li>", a += "<li><i class='pf pf-discover'></i></li>", a += "</ul></div>", t.after(a)
        }
    })
}(window, document);